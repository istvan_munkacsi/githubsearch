package com.istvan.munkacsi.githubuserssearch.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.Fragment
import com.istvan.munkacsi.githubuserssearch.main.activity.MainActivity

abstract class BaseFragment<V : ViewDataBinding, VM : BaseViewModel> : Fragment() {

    lateinit var binding: V

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        binding.apply {
            lifecycleOwner = this@BaseFragment
            setBindingVariables()
        }

        return binding.root
    }

    protected open fun setBindingVariables() {
        binding.setVariable(getViewModelVariableId(), getViewModel())
    }

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun getViewModel(): VM

    protected open fun getViewModelVariableId(): Int {
        return BR.vm
    }

    protected open fun getNavigatorVariableId(): Int? = null

    protected fun mainActivity() = activity as MainActivity?
}