package com.istvan.munkacsi.githubuserssearch.base.recycler_view_adapter

import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerViewAdapter<ViewHolder : RecyclerView.ViewHolder, Items> :
    RecyclerView.Adapter<ViewHolder>() {

    protected val items = mutableListOf<Items>()

    @Synchronized
    open fun setupItems(items: List<Items>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.count()
}