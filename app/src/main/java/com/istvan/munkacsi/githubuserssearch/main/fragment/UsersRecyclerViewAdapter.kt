package com.istvan.munkacsi.githubuserssearch.main.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.istvan.munkacsi.githubuserssearch.R
import com.istvan.munkacsi.githubuserssearch.base.recycler_view_adapter.QueryFilterRecyclerViewAdapter
import com.istvan.munkacsi.githubuserssearch.data.dto.User
import com.squareup.picasso.Picasso
import java.util.*
import javax.inject.Inject

class UsersRecyclerViewAdapter @Inject constructor(private val picasso: Picasso) :
    QueryFilterRecyclerViewAdapter<UsersRecyclerViewAdapter.GithubUserViewHolder, User>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GithubUserViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.main_fragment_github_user_recycler_view_item, parent, false)
        return GithubUserViewHolder(view)
    }

    override fun onBindViewHolder(holder: GithubUserViewHolder, position: Int) {
        val item = filteredItems[position]

        holder.apply {
            if(item.imageURL.isNotEmpty()) {
                picasso
                    .load(item.imageURL)
                    .placeholder(R.drawable.ic_launcher_background)
                    .into(avatarView)
            }
            loginView.text = item.login
            repositoriesView.setRepositoriesText(item.repos.map { it.name })
        }
    }

    private fun TextView.setRepositoriesText(repositoryList: List<String>) {
        val userRepositoriesPlural = context.resources.getQuantityText(
            R.plurals.github_user_repositories,
            repositoryList.size
        ).toString()

        text =
            String.format(userRepositoriesPlural, repositoryList.joinToString(", "))
    }

    override fun isMatchesQuery(query: String, item: User): Boolean {
        fun String.lc() = toLowerCase(Locale.getDefault())

        val lowerCaseQuery = query.lc()
        return query.isEmpty()
                || item.login.lc().startsWith(lowerCaseQuery)
                || item.repos.find { it.name.lc().startsWith(query) } != null
    }

    class GithubUserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val avatarView: ImageView = view.findViewById(R.id.avatarView)
        val loginView: TextView = view.findViewById(R.id.loginView)
        val repositoriesView: TextView = view.findViewById(R.id.repositoriesView)
    }
}