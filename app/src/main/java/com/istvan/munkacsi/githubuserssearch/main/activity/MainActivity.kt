package com.istvan.munkacsi.githubuserssearch.main.activity

import androidx.lifecycle.ViewModelProvider
import com.istvan.munkacsi.githubuserssearch.R
import com.istvan.munkacsi.githubuserssearch.base.activity.BaseActivity
import com.istvan.munkacsi.githubuserssearch.databinding.MainActivityBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<MainActivityBinding, MainViewModel>() {

    override fun getLayoutId() = R.layout.main_activity

    override fun getViewModel() = ViewModelProvider(this).get(MainViewModel::class.java)
}