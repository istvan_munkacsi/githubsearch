package com.istvan.munkacsi.githubuserssearch.data.dto.responses

import androidx.room.Entity

@Entity
data class RepoResponse(
    val name: String
)