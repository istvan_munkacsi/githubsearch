package com.istvan.munkacsi.githubuserssearch.main.fragment

import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModelProvider
import com.istvan.munkacsi.githubuserssearch.R
import com.istvan.munkacsi.githubuserssearch.base.fragment.BaseFragment
import com.istvan.munkacsi.githubuserssearch.databinding.MainFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment : BaseFragment<MainFragmentBinding, MainViewModel>() {

    @Inject
    lateinit var adapter: UsersRecyclerViewAdapter

    override fun getLayoutId() = R.layout.main_fragment

    override fun getViewModel() = ViewModelProvider(this).get(MainViewModel::class.java)

    override fun setBindingVariables() {
        super.setBindingVariables()
        binding.setVariable(BR.adapter, adapter)
    }
}
