package com.istvan.munkacsi.githubuserssearch.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.istvan.munkacsi.githubuserssearch.data.dto.entities.RepoDto
import com.istvan.munkacsi.githubuserssearch.data.dto.entities.UserDto
import com.istvan.munkacsi.githubuserssearch.data.room.AppDatabase.Companion.DATABASE_VERSION
import com.istvan.munkacsi.githubuserssearch.data.room.dao.UserDao

@Database(
    entities = [UserDto::class, RepoDto::class],
    version = DATABASE_VERSION,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "github_user_search_db"
    }

    abstract fun userDao(): UserDao
}