package com.istvan.munkacsi.githubuserssearch.base.recycler_view_adapter

import androidx.recyclerview.widget.RecyclerView

abstract class QueryFilterRecyclerViewAdapter<ViewHolder : RecyclerView.ViewHolder, Item> :
    BaseRecyclerViewAdapter<ViewHolder, Item>() {

    protected var filteredItems = listOf<Item>()
    private var query: String = ""

    fun setQuery(query: String) {
        this.query = query
        filteredItems = items.filter()
        notifyDataSetChanged()
    }

    override fun setupItems(items: List<Item>) {
        filteredItems = items.filter()
        super.setupItems(items)
    }

    fun List<Item>.filter(): List<Item> {
        return filter { isMatchesQuery(query, it) }
    }

    abstract fun isMatchesQuery(query: String, item: Item): Boolean

    override fun getItemCount() = filteredItems.size
}