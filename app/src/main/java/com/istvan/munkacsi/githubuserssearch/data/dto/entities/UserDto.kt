package com.istvan.munkacsi.githubuserssearch.data.dto.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class UserDto(

    @PrimaryKey
    val login: String,

    @SerializedName("avatar_url")
    val imageURL: String
)