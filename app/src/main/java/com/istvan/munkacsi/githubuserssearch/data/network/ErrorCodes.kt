package com.istvan.munkacsi.githubuserssearch.data.network

object ErrorCodes {

    const val UNAUTHORIZED = 401
    const val LIMIT_EXCEEDED = 403
}