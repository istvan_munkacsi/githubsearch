package com.istvan.munkacsi.githubuserssearch.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.istvan.munkacsi.githubuserssearch.data.dto.User
import com.istvan.munkacsi.githubuserssearch.data.dto.entities.RepoDto
import com.istvan.munkacsi.githubuserssearch.data.dto.entities.UserDto
import com.istvan.munkacsi.githubuserssearch.data.dto.responses.RepoResponse
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Dao
abstract class UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertUsers(users: List<UserDto>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertRepos(users: List<RepoDto>)

    fun insertUser(users: List<User>) {
        insertUsers(users.map { UserDto(it.login, it.imageURL) }).doOnComplete {
            for (user in users) {
                insertRepos(user.repos.map { RepoDto(it.name, user.login) })
            }
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe()
    }

    @Query("SELECT r.* FROM UserDto AS u INNER JOIN RepoDto AS r ON r.login = u.login WHERE r.login = :login")
    abstract fun getRepoDto(login: String): Single<List<RepoDto>>

    @Query("SELECT * FROM UserDto")
    abstract fun getUserDto(): Single<List<UserDto>>

    fun getUsers(): Single<Observable<User>> {
        return getUserDto().map { userDtoList ->
            Observable.fromIterable(userDtoList.map { it }).flatMap { user ->
                return@flatMap getRepoDto(user.login)
                    .map { User(user.login, user.imageURL, it.map { repoDto -> RepoResponse(repoDto.name) }) }
                    .toObservable()
            }
        }
    }
}