package com.istvan.munkacsi.githubuserssearch.main.activity

import androidx.hilt.lifecycle.ViewModelInject
import com.istvan.munkacsi.githubuserssearch.base.activity.BaseViewModel

class MainViewModel @ViewModelInject constructor() : BaseViewModel()