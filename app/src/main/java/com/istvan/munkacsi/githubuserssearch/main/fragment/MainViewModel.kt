package com.istvan.munkacsi.githubuserssearch.main.fragment

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.istvan.munkacsi.githubuserssearch.base.fragment.BaseViewModel
import com.istvan.munkacsi.githubuserssearch.data.dto.User
import io.reactivex.disposables.CompositeDisposable

class MainViewModel @ViewModelInject constructor(
    compositeDisposable: CompositeDisposable,
    private var interactor: MainInteractor
) :
    BaseViewModel(compositeDisposable) {

    val users = MutableLiveData<List<User>>()
    val dataState = MutableLiveData<DataState>().apply { postValue(DataState.INIT) }

    init {
        setupUsers()
    }

    private fun setupUsers() {
        dataState.postValue(DataState.LOADING)

        interactor.getUsers().observe({
            getUsersFromLocalDB()
        }, {
            it.observe(onError = {
                getUsersFromLocalDB()
            }, onComplete = { users ->
                if(users.isEmpty()) getUsersFromLocalDB()
                else {
                    dataState.postValue(DataState.SUCCESS)
                    interactor.insertToDB(users)
                    this.users.postValue(users)
                }
            })
        })
    }


    private fun getUsersFromLocalDB() {
        interactor.getUsersFromLocalDB().observe({
            dataState.postValue(DataState.ERROR)
        }, {
            it.observe(onError = {
                dataState.postValue(DataState.ERROR)
            }, onComplete = { users ->
                dataState.postValue(if (users.isEmpty()) DataState.EMPTY else DataState.SUCCESS)
                interactor.insertToDB(users)
                this.users.postValue(users)
            })
        })
    }
}