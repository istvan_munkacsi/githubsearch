package com.istvan.munkacsi.githubuserssearch.data.room

import android.content.Context
import androidx.room.Room
import com.istvan.munkacsi.githubuserssearch.data.room.dao.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppDatabaseModule {

    @Singleton
    @Provides
    fun providesGithubUserSearch(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            AppDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    fun providesGithubUserDao(appDatabase: AppDatabase): UserDao {
        return appDatabase.userDao()
    }
}