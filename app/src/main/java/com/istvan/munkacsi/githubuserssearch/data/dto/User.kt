package com.istvan.munkacsi.githubuserssearch.data.dto

import com.istvan.munkacsi.githubuserssearch.data.dto.responses.RepoResponse

data class User(
    val login: String,
    val imageURL: String,
    val repos: List<RepoResponse>
)