package com.istvan.munkacsi.githubuserssearch.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Application : Application()