package com.istvan.munkacsi.githubuserssearch.data.network.service

import com.istvan.munkacsi.githubuserssearch.data.dto.responses.RepoResponse
import com.istvan.munkacsi.githubuserssearch.data.dto.entities.UserDto
import com.istvan.munkacsi.githubuserssearch.data.network.Endpoints
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface UserService {

    @GET(Endpoints.USERS)
    fun getUsers(): Single<List<UserDto>>

    @GET(Endpoints.USER_REPOSITORIES)
    fun getUserRepositories(@Path(Endpoints.USER_LOGIN_PARAM) userLogin: String): Single<List<RepoResponse>>
}