package com.istvan.munkacsi.githubuserssearch.data.repository

import android.annotation.SuppressLint
import com.istvan.munkacsi.githubuserssearch.data.dto.User
import com.istvan.munkacsi.githubuserssearch.data.dto.entities.UserDto
import com.istvan.munkacsi.githubuserssearch.data.network.service.UserService
import com.istvan.munkacsi.githubuserssearch.data.room.dao.UserDao
import io.reactivex.Observable
import io.reactivex.Single
import java.lang.Exception
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val userDao: UserDao,
    private val userService: UserService
) {

    @SuppressLint("CheckResult")
    fun getUsers(): Single<Observable<User>> {
        return getUsersResponse().map { userResponses ->
            val userResponsesSplit =
                if (userResponses.size > 30) userResponses.subList(0, 30) else userResponses

            getReposResponse(userResponsesSplit).map { it }
                .apply { subscribe({}, { Single.error<Any>(it) }) }
        }.onErrorResumeNext(getUsersFromLocalDB())
    }

    fun insertToDB(users: List<User>) {
        userDao.insertUser(users)
    }

    fun getUsersFromLocalDB(): Single<Observable<User>> {
        return userDao.getUsers().onErrorResumeNext { Single.error(it) }
    }

    private fun getUsersResponse(): Single<List<UserDto>> {
        return userService.getUsers()
    }

    private fun getReposResponse(users: List<UserDto>): Observable<User> {
        return Observable.fromIterable(users.map { it }).flatMap { user ->
            return@flatMap userService.getUserRepositories(user.login).map {
                val reposSplit = if(it.size > 3) it.subList(0, 3) else it
                User(user.login, user.imageURL, reposSplit)
            }.toObservable()
        }
    }
}