package com.istvan.munkacsi.githubuserssearch.base.activity

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()