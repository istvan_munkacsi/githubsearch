package com.istvan.munkacsi.githubuserssearch.data.dto.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity
data class RepoDto(
    @PrimaryKey
    val name: String,

    @ForeignKey(
        entity = UserDto::class,
        parentColumns = ["login"],
        childColumns = ["login"],
        onDelete = ForeignKey.CASCADE
    )
    val login: String
)