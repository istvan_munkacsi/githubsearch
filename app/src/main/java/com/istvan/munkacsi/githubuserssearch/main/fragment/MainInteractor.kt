package com.istvan.munkacsi.githubuserssearch.main.fragment

import com.istvan.munkacsi.githubuserssearch.data.dto.User
import com.istvan.munkacsi.githubuserssearch.data.repository.UserRepository
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class MainInteractor @Inject constructor(private val userRepository: UserRepository) {

    fun getUsers(): Single<Observable<User>> {
        return userRepository.getUsers()
    }

    fun insertToDB(users: List<User>) {
        return userRepository.insertToDB(users)
    }

    fun getUsersFromLocalDB(): Single<Observable<User>> {
        return userRepository.getUsersFromLocalDB()
    }
}