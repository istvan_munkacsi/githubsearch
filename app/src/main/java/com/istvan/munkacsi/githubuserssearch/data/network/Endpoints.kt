package com.istvan.munkacsi.githubuserssearch.data.network

object Endpoints {

    const val BASE_URL = "https://api.github.com/"

    const val USERS = "users"

    const val USER_LOGIN_PARAM = "user_login"
    const val USER_REPOSITORIES = "$USERS/{$USER_LOGIN_PARAM}/repos"
}