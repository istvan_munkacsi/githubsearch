package com.istvan.munkacsi.githubuserssearch.base.binding_adapter

import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.istvan.munkacsi.githubuserssearch.base.fragment.BaseViewModel
import com.istvan.munkacsi.githubuserssearch.base.recycler_view_adapter.BaseRecyclerViewAdapter
import com.istvan.munkacsi.githubuserssearch.base.recycler_view_adapter.QueryFilterRecyclerViewAdapter

object BindingAdapter {

    @BindingAdapter(value = ["adapter", "items"], requireAll = true)
    @JvmStatic
    fun <Items> setItems(
        recyclerView: RecyclerView,
        adapter: BaseRecyclerViewAdapter<*, Items>,
        items: List<Items>?
    ) {
        if (recyclerView.adapter == null) {
            recyclerView.adapter = adapter
        }

        @Suppress("UNCHECKED_CAST")
        if (items != null) {
            adapter.setupItems(items)
        }
    }

    @BindingAdapter(value = ["emptyView", "loadingView", "errorView", "dataState"], requireAll = false)
    @JvmStatic
    fun manageViewState(
        view: View,
        emptyView: View?,
        loadingView: View?,
        errorView: View?,
        dataState: BaseViewModel.DataState?
    ) {
        loadingView?.visibility =
            if (dataState == BaseViewModel.DataState.LOADING) View.VISIBLE else View.GONE
        errorView?.visibility =
            if (dataState == BaseViewModel.DataState.ERROR) View.VISIBLE else View.GONE
        emptyView?.visibility =
            if (dataState == BaseViewModel.DataState.EMPTY) View.VISIBLE else View.GONE
    }

    @BindingAdapter(value = ["adapter2", "querySourceView"], requireAll = false)
    @JvmStatic
    fun textQuery(
        recyclerView: RecyclerView,
        adapter: QueryFilterRecyclerViewAdapter<*, *>,
        searchView: SearchView
    ) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    adapter.setQuery(newText)
                }
                if (recyclerView.adapter == null) {
                    recyclerView.adapter = adapter
                }

                return true
            }
        })
    }

}