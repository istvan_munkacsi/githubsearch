package com.istvan.munkacsi.githubuserssearch.base.fragment

import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

abstract class BaseViewModel(private var compositeDisposable: CompositeDisposable) : ViewModel() {

    enum class DataState {
        INIT, LOADING, ERROR, SUCCESS, EMPTY
    }

    fun <T> Single<T>.observe(onError: (Throwable) -> Unit, onSuccess: (T) -> Unit) {
        compositeDisposable.add(
            this.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ onSuccess(it) }, {
                    onError(it)
                })
        )
    }

    fun <T> Observable<T>.observe(onError: (Throwable) -> Unit, onComplete: (List<T>) -> Unit) {
        val values = mutableListOf<T>()
        compositeDisposable.add(
            this.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnNext { values.add(it) }
                .doOnComplete { onComplete(values) }
                .subscribe({}, {
                    onError(it)
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}